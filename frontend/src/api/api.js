const fake_api = {
    users: [
        {
            id: 1,
            username: 'tim',
            email: 'tim.esadf@safd.de',
            type: 'admin',
            isActive: true,
        },
        {
            id: 2,
            username: 'John',
            email: 'john.',
            type: 'user',
            isActive: false,
        }
    ],
    games: [
        {
            id: "g1", 
            game_name: "Quiz mit den Jungs!", 
            game_desc:"Für unser kleines Quiz Spiel habe ich die folgende Seite programmiert. Sollte es zu Problemen kommen, schickt mir am besten einen Screenshot.", 
            game_state:3
        },
        {
            id: "g2", 
            game_desc:"Lici will spielen", 
            game_name: "Quiz mit Lici!", 
            game_state:2
        },
        {
            id: "g3", 
            game_desc:"Lici will spielen", 
            game_name: "Quiz mit Lici!", 
            game_state:1
        },

    ],
    rounds: [
        {
            id: "r1",
            round_name: "Maya 2020",
            round_desc: "asdfasdff",
            round_state: 3,
            belongs_to_game_id: "g1",
        },
        {
            id: "r2",
            round_name: "Atzteken 2012",
            round_desc: "asdsdaf",
            round_state: 3,
            belongs_to_game_id: "g1",
        },
        {
            id: "r3",
            round_name: "asdf 2012",
            round_desc: "asdsdaf",
            round_state: 1,
            belongs_to_game_id: "g1",
        },
        {
            id: "r4",
            round_name: "asdf 2012",
            round_desc: "asdsdaf",
            round_state: 3,
            belongs_to_game_id: "g2",
        }
    ],
    questions: [
        {
            id: "q1",
            question_text: "Wann war die letzte WM?",
            question_type: 1,
            question_state: 3,
            belongs_to_round_id: "r1"
        }
    ],
    answers: [
        {
            id: "a1",
            answer_text: "2018",
            is_answer_right: true,
            belongs_to_question_id: "q1"
        },
        {
            id: "a2",
            answer_text: "2016",
            is_answer_right: false,
            belongs_to_question_id: "q1"
        },
        {
            id: "a3",
            answer_text: "2014",
            is_answer_right: false,
            belongs_to_question_id: "q1"
        },
        {
            id: "a4",
            answer_text: "2020",
            is_answer_right: false,
            belongs_to_question_id: "q1"
        }
    ]
}

const api = {
    user: {
        getUserByUsername: function (username) {
            if (username.username === fake_api.users[0].username) {
                return fake_api.users[0]
            } else {
                return ""
            }
        },
        isActive: function (username) {
            if (username === fake_api.users[0].username) {
                return fake_api.users[0].isActive
            } else {
                return false
            }
        },
        createTempUser: function (username) {
            let isUserExisting = false;
            fake_api.users.forEach(user => {
                if (user.username == username.username) {
                    isUserExisting += 1;
                }
            });

            if (isUserExisting != 0 ) {
                return {
                    err: "The user is already existing"
                }
            } else {
                var newUser = {
                    id: 11,
                    username: username.username,
                    email: 'tempUser@email.com',
                    type: 'user',
                    isActive: true,
                }

                fake_api.users.push(newUser)
                return {
                    err: "",
                    user: newUser
                }
            }
        }
    },
    game: {
        getGameById: function(game_id) {
            for (var i = 0; i < fake_api.games.length; i++) {
                var game = fake_api.games[i];

                if (game.id == game_id) {
                    return game
                } 
            }
            return "";
        }
    },
    round: {
        getRoundsByGameId: function(game_id) {
            for (var i = 0; i < fake_api.rounds.length; i++) {
                var round = fake_api.rounds[i];
                var rounds = [];

                if (round.belongs_to_game_id == game_id) {
                    rounds.push(round)
                } 
            }
            return rounds
        }
    },
    question: {
        getQuestionByRoundId: function(round_id) {
            for (var i = 0; i < fake_api.questions.length; i++) {
                var question = fake_api.questions[i];
                var questions = [];

                if (question.belongs_to_round_id == round_id) {
                    questions.push(question)
                } 
            }
            return questions
        }
    },
    answer: {
        getAnswerByRoundId: function(question_id) {
            for (var i = 0; i < fake_api.answers.length; i++) {
                var answer = fake_api.answers[i];
                var answers = [];

                if (answer.belongs_to_question_id == question_id) {
                    answers.push(answer)
                } 
            }
            return answers;
        }
    }
};

export default api;