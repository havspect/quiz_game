import { createWebHistory, createRouter } from "vue-router";
// General pages
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import NotFound from "../views/NotFound.vue"

// imports for a new quiz
import Quiz from "../views/Quiz.vue"


// imports for game
import Game from "../views/Game.vue"
import Round from "../components/Game/RoundTemp.vue"
import Question from "../components/Game/QuestionTemp.vue"
import Waiting from "../components/Game/WaitingScreen.vue"
import RoundOverview from "../components/Game/OverviewTemp.vue"

import api from "../api/api";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/:catchAll(.*)",
    component: NotFound,
  },
  {
    path: "/game/:id",
    component: Game,
    name: "game",
    redirect: {name: "waiting"},
    meta: { isGame: true },
    children: [
      {
        path: "waiting",
        component: Waiting,
        name: "waiting",
      },
      {
        path: "overview",
        component: RoundOverview,
        name: "overview",
      },
      {
        path: "round",
        component: Round,
        name: "round",
        props: route => ({round_id: route.query.id}),
        meta: { isRound: true, isRoundStart: true, requiresAuth: true},
        children: [
          {
            path:"question",
            component: Question,
            props: route => ({question_id: route.query.id}),
            meta: { isQuestion: true, isQuestionStart: true},
          }
        ]
      }
    ]
  },
  {
    path: "/quiz/:quiz_id",
    component: Quiz,
    name: "quiz",
    meta: { requiresAuth: true }
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (false) {
      next({
        path: from.path,
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.isGame)) {
    // route requires an existing game
    // if not, redirect to home page.
    if (api.game.getGameById(to.params.id) === "") {
      console.log(api.game.getGameById(to.params.id))
      next({
        name: "Home",
        query: { redirect: to.fullPath }
      })
    } else {
      console.log("Hallo")
      next()
    }
  } else {
    next() // make sure to always call next()!
  } 
})

export default router;