import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import router from './router'

// set auth header
// Axios.defaults.headers.common['Authorization'] = `Bearer ${store.state.token}`;

createApp(App).use(router).mount('#app')

