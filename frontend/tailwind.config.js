module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        black: "#000000",
        blue: {
          light: '#1e325c',
          DEFAULT: '#14213d',
          dark: '#0a111f',
        },
        orange: {
          light: '#fdb849',
          DEFAULT: '#fca311',
          dark: '#f29602',
        },
        platinum: {
          light: '#ebebeb',
          DEFAULT: '#d6d6d6',
          dark: '#c2c2c2',
        },
        white: '#ffffff',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
