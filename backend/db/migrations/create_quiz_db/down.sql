-- This file should undo anything in `up.sql`
DROP TABLE "users", "quizzes", "rounds", "questions", "answers", "games", "user_games", "question_game" CASCADE;