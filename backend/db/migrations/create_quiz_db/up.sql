-- Your SQL goes here

CREATE TABLE "users" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "username" varchar(255) UNIQUE NOT NULL,
  "email" varchar(255) UNIQUE NOT NULL,
  "password" varchar(255) NOT Null,
  "is_admin" boolean NOT NULL DEFAULT false,
  "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE "quizzes" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "quizname" varchar UNIQUE NOT NULL,
  "description" text,
  "user_id" uuid NOT NULL,
  "is_public" boolean NOT NULL DEFAULT true
);

CREATE TABLE "rounds" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "round_name" varchar NOT NULL,
  "quiz_id" uuid NOT NULL,
  "user_id" uuid NOT NULL,
  "timer_length" int NOT NULL DEFAULT 30
);

CREATE TABLE "questions" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "round_id" uuid NOT NULL,
  "question_type" int NOT NULL DEFAULT 0,
  "question_text" text NOT NULL,
  "question_category" text
);

CREATE TABLE "answers" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "question_id" uuid NOT NULL,
  "answer_text" text NOT NULL,
  "is_answer_right" boolean NOT NULL DEFAULT false
);

CREATE TABLE "games" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "game_name" varchar NOT NULL,
  "state" int NOT NULL DEFAULT 0,
  "quiz_id" uuid NOT NULL
);

CREATE TABLE "user_games" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "user_id" uuid NOT NULL,
  "game_id" uuid NOT NULL,
  "round_id" uuid,
  "round_state" int DEFAULT 0,
  "points" int DEFAULT 0,
  "position" int DEFAULT 0,

  UNIQUE("user_id", "game_id")
);

CREATE TABLE "question_game" (
  "id" uuid PRIMARY KEY DEFAULT gen_random_uuid (),
  "user_game_id" uuid NOT NULL,
  "question_id" uuid NOT NULL,
  "answer_id" uuid,
  
  UNIQUE ("user_game_id", "question_id")
);

ALTER TABLE "question_game" ADD FOREIGN KEY ("user_game_id") REFERENCES "user_games" ("id");
ALTER TABLE "question_game" ADD FOREIGN KEY ("question_id") REFERENCES "questions" ("id");
ALTER TABLE "question_game" ADD FOREIGN KEY ("answer_id") REFERENCES "answers" ("id");

ALTER TABLE "quizzes" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "rounds" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "questions" ADD FOREIGN KEY ("round_id") REFERENCES "rounds" ("id");

ALTER TABLE "answers" ADD FOREIGN KEY ("question_id") REFERENCES "questions" ("id");


ALTER TABLE "games" ADD FOREIGN KEY ("quiz_id") REFERENCES "quizzes" ("id");

ALTER TABLE "user_games" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "user_games" ADD FOREIGN KEY ("game_id") REFERENCES "games" ("id");
ALTER TABLE "user_games" ADD FOREIGN KEY ("round_id") REFERENCES "rounds" ("id");
