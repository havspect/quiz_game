package postgres

import (
	"fmt"
	"quiz_backend/models/quiz"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

func NewQuizStore(db *sqlx.DB) *QuizStore {
	return &QuizStore{
		DB: db,
	}
}

type QuizStore struct {
	*sqlx.DB
}

func (s *QuizStore) Quiz(id uuid.UUID) (quiz.Quiz, error) {
	var q quiz.Quiz
	if err := s.Get(&q, `SELECT * FROM quizzes WHERE id = $1`, id); err != nil {
		return quiz.Quiz{}, fmt.Errorf("error getting quiz: %w", err)
	}
	return q, nil
}

func (s *QuizStore) QuizzesByUserID(userID uuid.UUID) ([]quiz.Quiz, error) {
	var qq []quiz.Quiz
	if err := s.Select(&qq, `SELECT * FROM quizzes WHERE user_id = $1`, userID); err != nil {
		return []quiz.Quiz{}, fmt.Errorf("error getting quizzesbyUid: %w", err)
	}
	return qq, nil
}

func (s *QuizStore) Quizzes() ([]quiz.Quiz, error) {
	var qq []quiz.Quiz
	if err := s.Select(&qq, `SELECT * FROM quizzes`); err != nil {
		return []quiz.Quiz{}, fmt.Errorf("error getting quizzes: %w", err)
	}
	return qq, nil
}

func (s *QuizStore) CreateQuiz(q *quiz.Quiz) error {
	if err := s.Get(q, `INSERT INTO quizzes (quizname, description, user_id, is_public) VALUES ($1, $2, $3, $4) RETURNING *`,
		q.Quizname,
		q.Desc,
		q.UserID,
		q.IsPublic,
	); err != nil {
		return fmt.Errorf("error creating quiz: %w", err)
	}
	return nil
}

func (s *QuizStore) UpdateQuiz(q *quiz.Quiz) error {
	if err := s.Get(q, `UPDATE quizzes SET quizname = $1, description = $2, is_public = $3 WHERE id = $4 RETURNING *`,
		q.Quizname,
		q.Desc,
		q.IsPublic,
		q.ID,
	); err != nil {
		return fmt.Errorf("error updating quiz: %w", err)
	}
	return nil
}

func (s *QuizStore) DeleteQuiz(id uuid.UUID) error {
	if _, err := s.Exec(`DELETE FROM quizzes WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting quiz: %w", err)
	}
	return nil
}
