package postgres

import (
	"fmt"
	"quiz_backend/models/game"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// NewGameStore exported
func NewGameStore(db *sqlx.DB) *GameStore {
	return &GameStore{
		DB: db,
	}
}

// GameStore struct exported
type GameStore struct {
	*sqlx.DB
}

// Game ..
func (s *GameStore) Game(id uuid.UUID) (game.Game, error) {
	var q game.Game
	if err := s.Get(&q, `SELECT * FROM games WHERE id = $1`, id); err != nil {
		return game.Game{}, fmt.Errorf("error getting game: %w", err)
	}
	return q, nil
}

// Games ...
func (s *GameStore) Games() ([]game.Game, error) {
	var qq []game.Game
	if err := s.Select(&qq, `SELECT * FROM games`); err != nil {
		return []game.Game{}, fmt.Errorf("error getting games: %w", err)
	}
	return qq, nil
}

// CreateGame ...
func (s *GameStore) CreateGame(q *game.Game) error {
	if err := s.Get(q, `INSERT INTO games (game_name, state, quiz_id) VALUES ($1, $2, $3) RETURNING *`,
		q.GameName,
		q.State,
		q.QuizID,
	); err != nil {
		return fmt.Errorf("error creating games: %w", err)
	}
	return nil
}

// UpdateGame ...
func (s *GameStore) UpdateGame(q *game.Game) error {
	if err := s.Get(q, `UPDATE games SET game_name = $1, state = $2, round_id = $3, round_state = $4, quiz_id = $5 WHERE id = $6 RETURNING *`,
		q.GameName,
		q.State,
		q.RoundID,
		q.RoundState,
		q.QuizID,
		q.ID,
	); err != nil {
		return fmt.Errorf("error updating game: %w", err)
	}
	return nil
}

// DeleteGame ...
func (s *GameStore) DeleteGame(id uuid.UUID) error {
	if _, err := s.Exec(`DELETE FROM games WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting game: %w", err)
	}
	return nil
}
