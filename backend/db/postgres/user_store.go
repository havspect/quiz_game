package postgres

import (
	"errors"
	"fmt"
	"log"

	"quiz_backend/models/auth"
	"quiz_backend/models/user"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"golang.org/x/crypto/bcrypt"
)

//NewUserStore exported
func NewUserStore(db *sqlx.DB) *UserStore {
	return &UserStore{
		DB: db,
	}
}

//UserStore exported
type UserStore struct {
	*sqlx.DB
}

//User returns only one user
func (s *UserStore) User(id uuid.UUID) (user.User, error) {
	var u user.User
	if err := s.Get(&u, `SELECT * FROM users WHERE id = $1`, id); err != nil {
		return user.User{}, fmt.Errorf("Error getting user: %w", err)
	}
	return u, nil
}

//Users returns a list of all users
func (s *UserStore) Users() ([]user.User, error) {
	var users []user.User
	if err := s.Select(&users, `SELECT * FROM users ORDER BY username ASC`); err != nil {
		return []user.User{}, fmt.Errorf("Error getting all users: %w", err)
	}
	return users, nil
}

//DeleteUser with a given uuid from the database
func (s *UserStore) DeleteUser(id uuid.UUID) error {
	if _, err := s.Exec(`DELETE FROM users WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting user: %w", err)
	}
	return nil
}

var authModel = new(auth.AuthModel)

//Login ...
func (s *UserStore) Login(form user.LoginForm) (user user.User, token auth.Token, err error) {
	err = s.Get(&user, "SELECT * FROM users WHERE email=LOWER($1) LIMIT 1", form.Email)

	if err != nil {
		return user, token, err
	}

	//Compare the password form and database if match
	bytePassword := []byte(form.Password)
	byteHashedPassword := []byte(user.Password)

	err = bcrypt.CompareHashAndPassword(byteHashedPassword, bytePassword)

	if err != nil {
		return user, token, errors.New("Invalid password")
	}

	//Generate the JWT auth token
	tokenDetails, err := authModel.CreateToken(user.ID)

	log.Println(tokenDetails)

	saveErr := authModel.CreateAuth(user.ID, tokenDetails)
	if saveErr == nil {
		token.AccessToken = tokenDetails.AccessToken
		token.RefreshToken = tokenDetails.RefreshToken
	}

	return user, token, nil
}

//Register ...
func (s *UserStore) Register(form user.RegisterForm) (user user.User, err error) {
	var checkUser int
	//Check if the user exists in databas
	if err := s.Get(&checkUser, "SELECT count(id) FROM users WHERE email=LOWER($1) LIMIT 1", form.Email); err != nil {
		return user, err
	}

	if checkUser > 0 {
		return user, errors.New("User already exists")
	}

	bytePassword := []byte(form.Password)
	hashedPassword, err := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	if err != nil {
		panic(err) //Something really went wrong here...
	}

	//Create the user and return back the user ID
	err = s.Get(&user, "INSERT INTO users (email, password, username) VALUES ($1, $2, $3) RETURNING *", form.Email, string(hashedPassword), form.Name)

	return user, err
}
