package postgres

import (
	"fmt"
	"quiz_backend/models/question"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// NewQuestionStore exported
func NewQuestionStore(db *sqlx.DB) *QuestionStore {
	return &QuestionStore{
		DB: db,
	}
}

// QuestionStore struct exported
type QuestionStore struct {
	*sqlx.DB
}

// Question exported
// Returns question for given id
func (s *QuestionStore) Question(id uuid.UUID) (question.Question, error) {
	var q question.Question
	if err := s.Get(&q, `SELECT * FROM questions WHERE id = $1`, id); err != nil {
		return question.Question{}, fmt.Errorf("error getting question: %w", err)
	}
	return q, nil
}

// QuestionsByRoundID exported
// Returns all questions by roundID
func (s *QuestionStore) QuestionsByRoundID(roundID uuid.UUID) ([]question.Question, error) {
	var qq []question.Question
	if err := s.Select(&qq, `SELECT * FROM questions WHERE round_id = $1`, roundID); err != nil {
		return []question.Question{}, fmt.Errorf("error getting quizzesbyUid: %w", err)
	}
	return qq, nil
}

// Questions exported
// Reutrns all questions
func (s *QuestionStore) Questions() ([]question.Question, error) {
	var qq []question.Question
	if err := s.Select(&qq, `SELECT * FROM questions`); err != nil {
		return []question.Question{}, fmt.Errorf("error getting quizzesbyUid: %w", err)
	}
	return qq, nil
}

// CreateQuestion exported
// Gets question and saves the question in the database
func (s *QuestionStore) CreateQuestion(q *question.Question) error {
	if err := s.Get(q, `INSERT INTO questions (question_type, question_text, question_category, round_id) VALUES ($1, $2, $3, $4) RETURNING *`,
		q.QuestionType,
		q.QuestionText,
		q.QuestionCategory,
		q.RoundID,
	); err != nil {
		return fmt.Errorf("error creating question: %w", err)
	}
	return nil
}

// UpdateQuestion exported
func (s *QuestionStore) UpdateQuestion(q *question.Question) error {
	if err := s.Get(q, `UPDATE questions SET question_type = $1, question_text = $2, question_category = $3, round_id = $4 WHERE id = $5 RETURNING *`,
		q.QuestionType,
		q.QuestionText,
		q.QuestionCategory,
		q.RoundID,
		q.ID,
	); err != nil {
		return fmt.Errorf("error updating question: %w", err)
	}
	return nil
}

// DeleteQuestion exported
func (s *QuestionStore) DeleteQuestion(id uuid.UUID) error {
	if _, err := s.Exec(`DELETE FROM questions WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting question: %w", err)
	}
	return nil
}
