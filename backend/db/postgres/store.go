package postgres

import (
	"fmt"
	"log"
	"os"
	"quiz_backend/models/answer"
	"quiz_backend/models/question"
	"quiz_backend/models/quiz"
	"quiz_backend/models/round"
	"quiz_backend/models/user"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq" // db driver
)

//StoreObj -> exported for further usage
var StoreObj *Store

func init() {
	if err := godotenv.Load(".env"); err != nil {
		log.Fatal("Error loading .env File: %w", err)
	}
	postgresURL := os.Getenv("POSTGRES_URL")

	// Establish Store
	var err error
	StoreObj, err = NewStore(postgresURL)
	if err != nil {
		log.Fatal(err)
	}

}

//NewStore exported
func NewStore(dataSourceName string) (*Store, error) {
	db, err := sqlx.Open("postgres", dataSourceName)
	if err != nil {
		return nil, fmt.Errorf("error opening database: %w", err)
	}
	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("Connection to database: %w", err)
	}
	return &Store{
		UserStore:     NewUserStore(db),
		QuizStore:     NewQuizStore(db),
		RoundStore:    NewRoundStore(db),
		QuestionStore: NewQuestionStore(db),
		AnswerStore:   NewAnswerStore(db),
	}, nil
}

//Store The store contains every score for a new model
type Store struct {
	user.UserStore
	quiz.QuizStore
	round.RoundStore
	question.QuestionStore
	answer.AnswerStore
}
