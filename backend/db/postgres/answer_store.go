package postgres

import (
	"fmt"
	"quiz_backend/models/answer"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

// NewAnswerStore exported
func NewAnswerStore(db *sqlx.DB) *AnswerStore {
	return &AnswerStore{
		DB: db,
	}
}

// AnswerStore struct exported
type AnswerStore struct {
	*sqlx.DB
}

// Answer exported
func (s *AnswerStore) Answer(id uuid.UUID) (answer.Answer, error) {
	var q answer.Answer
	if err := s.Get(&q, `SELECT * FROM answers WHERE id = $1`, id); err != nil {
		return answer.Answer{}, fmt.Errorf("error getting answer: %w", err)
	}
	return q, nil
}

// AnswersByQuestionID exported
func (s *AnswerStore) AnswersByQuestionID(questionID uuid.UUID) ([]answer.Answer, error) {
	var qq []answer.Answer
	if err := s.Select(&qq, `SELECT * FROM answers WHERE question_id = $1`, questionID); err != nil {
		return []answer.Answer{}, fmt.Errorf("error getting quizzesbyUid: %w", err)
	}
	return qq, nil
}

// Answers exported
func (s *AnswerStore) Answers() ([]answer.Answer, error) {
	var qq []answer.Answer
	if err := s.Select(&qq, `SELECT * FROM answers`); err != nil {
		return []answer.Answer{}, fmt.Errorf("error getting quizzes: %w", err)
	}
	return qq, nil
}

// CreateAnswer exported
func (s *AnswerStore) CreateAnswer(q *answer.Answer) error {
	if err := s.Get(q, `INSERT INTO answers (answer_text, is_answer_right, question_id) VALUES ($1, $2, $3) RETURNING *`,
		q.AnswerText,
		q.IsAnswerRight,
		q.QuestionID,
	); err != nil {
		return fmt.Errorf("error creating answer: %w", err)
	}
	return nil
}

// UpdateAnswer exported
func (s *AnswerStore) UpdateAnswer(q *answer.Answer) error {
	if err := s.Get(q, `UPDATE answers SET answer_text = $1, is_answer_right = $2, question_id = $3 WHERE id = $4 RETURNING *`,
		q.AnswerText,
		q.IsAnswerRight,
		q.QuestionID,
		q.ID,
	); err != nil {
		return fmt.Errorf("error updating answer: %w", err)
	}
	return nil
}

// DeleteAnswer exported
func (s *AnswerStore) DeleteAnswer(id uuid.UUID) error {
	if _, err := s.Exec(`DELETE FROM answers WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting answer: %w", err)
	}
	return nil
}

// CheckIfAnswersAreValid exported
// Check if the number of correct answers is coresponding to the question type
func (s *AnswerStore) CheckIfAnswersAreValid(questionID uuid.UUID) error {
	aa, err := s.AnswersByQuestionID(questionID)
	if err != nil {
		return err
	}

	q, err := StoreObj.QuestionStore.Question(questionID)
	if err != nil {
		return err
	}

	switch q.QuestionType {
	case 0:
		numberOfCorrectAnswers := 0
		for _, a := range aa {
			if a.IsAnswerRight {
				numberOfCorrectAnswers++
			}
		}

		if numberOfCorrectAnswers != 1 {
			err := fmt.Errorf("Error: Number of correct answers is unequal 1")
			return err
		}
		return nil
	case 1, 2:
		if len(aa) != 1 {
			err := fmt.Errorf("Error: Number of questions is not aloowed for question type guess")
			return err
		}
		return nil
	default:
		return nil
	}
}

// CheckIfAnswerIsCorrect exported
// Returns nil if answer is correct
func (s *AnswerStore) CheckIfAnswerIsCorrect(questionID uuid.UUID, answerID uuid.UUID) (bool, error) {
	q, err := StoreObj.QuestionStore.Question(questionID)
	if err != nil {
		return false, err
	}

	switch q.QuestionType {
	case 0:
		a, err := s.Answer(answerID)
		if err != nil {
			return false, err
		}
		if a.IsAnswerRight {
			return true, nil
		}
		return false, nil
	case 1:
		panic("hallo")
	default:
		return false, fmt.Errorf("Failure")
	}

}
