package postgres

import (
	"fmt"
	"quiz_backend/models/round"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

//NewRoundStore exported
func NewRoundStore(db *sqlx.DB) *RoundStore {
	return &RoundStore{
		DB: db,
	}
}

//RoundStore exported
type RoundStore struct {
	*sqlx.DB
}

// Round exported
func (r *RoundStore) Round(id uuid.UUID) (round.Round, error) {
	var ro round.Round
	if err := r.Get(&ro, `SELECT * FROM rounds WHERE id = $1`, id); err != nil {
		return round.Round{}, fmt.Errorf("Error getting round: %w", err)
	}
	return ro, nil
}

// RoundsByQuizID exported
func (r *RoundStore) RoundsByQuizID(quizID uuid.UUID) ([]round.Round, error) {
	var rr []round.Round
	if err := r.Select(&rr, `SELECT * FROM rounds WHERE quiz_id = $1`, quizID); err != nil {
		return []round.Round{}, fmt.Errorf("error getting rounds: %w", err)
	}
	return rr, nil
}

// RoundsByUserID exported
// Recives userID, returns list of rounds belonging to the user
func (r *RoundStore) RoundsByUserID(userID uuid.UUID) ([]round.Round, error) {
	var rr []round.Round
	if err := r.Select(&rr, `SELECT * FROM rounds WHERE user_id = $1`, userID); err != nil {
		return []round.Round{}, fmt.Errorf("error getting rounds: %w", err)
	}
	return rr, nil
}

// Rounds exported
// Returns all rounds
func (r *RoundStore) Rounds() ([]round.Round, error) {
	var rr []round.Round
	if err := r.Select(&rr, `SELECT * FROM rounds`); err != nil {
		return []round.Round{}, fmt.Errorf("error getting rounds: %w", err)
	}
	return rr, nil
}

// CreateRound exported
// Save a round to the database
func (r *RoundStore) CreateRound(q *round.Round) error {
	if err := r.Get(q, `INSERT INTO rounds (round_name, user_id, quiz_id) VALUES ($1, $2, $3) RETURNING *`,
		q.Roundname,
		q.UserID,
		q.QuizID,
	); err != nil {
		return fmt.Errorf("error creating round: %w", err)
	}
	return nil
}

// UpdateRound exported
// Update exisiting round
func (r *RoundStore) UpdateRound(q *round.Round) error {
	if err := r.Get(q, `UPDATE rounds SET round_name = $1, user_id = $2, quiz_id = $3 WHERE id = $4 RETURNING *`,
		q.Roundname,
		q.UserID,
		q.QuizID,
		q.ID,
	); err != nil {
		return fmt.Errorf("error updating round: %w", err)
	}
	return nil
}

// DeleteRound exported
// Delete round with specified id
func (r *RoundStore) DeleteRound(id uuid.UUID) error {
	if _, err := r.Exec(`DELETE FROM rounds WHERE id = $1`, id); err != nil {
		return fmt.Errorf("error deleting round: %w", err)
	}
	return nil
}
