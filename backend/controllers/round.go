package controllers

import (
	"fmt"
	"net/http"
	"quiz_backend/db/postgres"
	"quiz_backend/models/round"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// GetRound exported
// Handler for the Get[/round] route
func GetRound(c *gin.Context) {
	id := c.Query("round_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID, err := uuid.Parse(id)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	obj, err := postgres.StoreObj.RoundStore.Round(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// GetRounds exported
// Handler for the Get [/rounds] route
func GetRounds(c *gin.Context) {
	obj, err := postgres.StoreObj.RoundStore.Rounds()
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// GetRoundsByUserID exported
// Handler for the GET [/user/:user_id/rounds]
func GetRoundsByUserID(c *gin.Context) {
	id := c.Param("user_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in params."})
		return
	}

	uuidID, err := uuid.Parse(id)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err})
		return
	}

	obj, err := postgres.StoreObj.RoundStore.RoundsByUserID(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})

}

// GetRoundsByQuizID exported
// Handler for the GET [/quiz/:quiz_id/rounds]
func GetRoundsByQuizID(c *gin.Context) {
	id := c.Query("quiz_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in params."})
		return
	}

	uuidID, err := uuid.Parse(id)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	obj, err := postgres.StoreObj.RoundStore.RoundsByQuizID(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// CreateRound exported
// Handler for POST [/round] - data {roundname: "...", quiz_id="...", user_id="..."}
func CreateRound(c *gin.Context) {
	var f round.FormRound

	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	r, err := f.ToRound()
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Wrong input format!"})
		return
	}

	if err := postgres.StoreObj.RoundStore.CreateRound(&r); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": r})
}

// UpdateRound exported
// Handler for PATCH [/round] - data {"id": "xxx", "round_name": "xxx", "user_id": "xxx", "quiz_id": "xxx"}
func UpdateRound(c *gin.Context) {
	var f round.Round

	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	if err := postgres.StoreObj.RoundStore.UpdateRound(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": f})
}

// DeleteRound exported
// Handler for DELETE [/round] -> /round?round_id="xxx-xxx-xxxx"
func DeleteRound(c *gin.Context) {
	id := c.Query("round_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID := GetUUIDfromString(id, c)

	if err := postgres.StoreObj.RoundStore.DeleteRound(uuidID); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	msg := fmt.Sprintf("Round %s is delted!", id)
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": msg})
}
