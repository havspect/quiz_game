package controllers

import (
	"fmt"
	"net/http"
	"quiz_backend/db/postgres"
	"quiz_backend/models/answer"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// AnswerController exported for seak of simplicity
type AnswerController struct{}

// GetAnswersByQuestionID exported
// Handler for GET [/question/answers?question_id="xxx"]
func (a *AnswerController) GetAnswersByQuestionID(c *gin.Context) {
	id := c.Query("question_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID := GetUUIDfromString(id, c)

	obj, err := postgres.StoreObj.AnswerStore.AnswersByQuestionID(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// CheckAnswer exported
// Handler for GET [/question/checkAnswer?answer_id=xxx&question_id="xxx"]
func (a *AnswerController) CheckAnswer(c *gin.Context) {
	answerID := c.Query("answer_id")
	questionID := c.Query("question_id")
	if answerID == "" || questionID == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidAnswer, AnswerErr := uuid.Parse(answerID)
	uuidQuestion, QuestionErr := uuid.Parse(questionID)
	if AnswerErr != nil || QuestionErr != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Error parsing value to uuid"})
		return
	}

	isRight, err := postgres.StoreObj.AnswerStore.CheckIfAnswerIsCorrect(uuidQuestion, uuidAnswer)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"ok": false, "err": "Error checking answer"})
		return
	}
	if isRight {
		c.JSON(http.StatusOK, gin.H{"ok": true, "response": gin.H{"is_answer_correct": true}})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": gin.H{"is_answer_correct": false}})
}

// CreateAnswer exported
// Handler for POST [/answer]
func (a *AnswerController) CreateAnswer(c *gin.Context) {
	var f answer.FormAnswer

	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	r, err := f.ToAnswer()
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Wrong input format!"})
		return
	}

	if err := postgres.StoreObj.AnswerStore.CreateAnswer(&r); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"ok": true, "response": r})

}

// UpdateAnswer exported
// Handler for [/answer]
func (a *AnswerController) UpdateAnswer(c *gin.Context) {
	var f answer.Answer

	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	if err := postgres.StoreObj.AnswerStore.UpdateAnswer(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": f})
}

// DeleteAnswer exported
// Handler for [/answer]
func (a *AnswerController) DeleteAnswer(c *gin.Context) {
	id := c.Query("question_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID := GetUUIDfromString(id, c)

	if err := postgres.StoreObj.AnswerStore.DeleteAnswer(uuidID); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	msg := fmt.Sprintf("Question %s is delted!", id)
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": msg})
}
