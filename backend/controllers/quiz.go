package controllers

import (
	"fmt"
	"net/http"
	"quiz_backend/db/postgres"
	"quiz_backend/models/quiz"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func GetQuiz(c *gin.Context) {
	quizID := c.Query("quiz_id")
	if quizID == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty Quiz id in query params."})
		return
	}

	uuidQuiz, err := uuid.Parse(quizID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err})
		return
	}

	if quiz, err := postgres.StoreObj.QuizStore.Quiz(uuidQuiz); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("DB Error: %w!", err)})
		return
	} else {
		c.JSON(http.StatusOK, gin.H{"ok": true, "response": quiz})
	}
}

func GetQuizzes(c *gin.Context) {
	if qq, err := postgres.StoreObj.QuizStore.Quizzes(); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("DB Error: %w!", err)})
	} else {
		c.JSON(http.StatusOK, gin.H{"ok": true, "response": qq})
	}
}

func GetQuizzesByUserId(c *gin.Context) {
	userID := c.Param("user_id")
	if userID == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty User id in params."})
		return
	}

	uuidUser := GetUUIDfromString(userID, c)

	if qq, err := postgres.StoreObj.QuizStore.QuizzesByUserID(uuidUser); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("DB Error: %w!", err)})
	} else {
		c.JSON(http.StatusOK, gin.H{"ok": true, "response": qq})
	}
}

func UpdateQuiz(c *gin.Context) {
	panic("ha") //TODO: implement
}

func PostQuiz(c *gin.Context) {
	var quiz quiz.FormQuiz

	if c.BindJSON(&quiz) != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
	}

	newQuiz, err := quiz.ToQuiz()
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("Conversion Error: %w", err)})
		return
	}

	if err := postgres.StoreObj.QuizStore.CreateQuiz(&newQuiz); err != nil {
		c.JSON(http.StatusRequestedRangeNotSatisfiable, gin.H{"ok": false, "err": fmt.Sprintf("DB Error: %w", err)})
		return
	}

	c.JSON(http.StatusOK, gin.H{"ok": true, "response": newQuiz})
}

func DeleteQuiz(c *gin.Context) {
	id := c.Query("quiz_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty Quiz id in query params."})
		return
	}

	uuidQuiz := GetUUIDfromString(id, c)

	if err := postgres.StoreObj.QuizStore.DeleteQuiz(uuidQuiz); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("DB Error: %w!", err)})
		return
	}

	msg := fmt.Sprintf("Quiz %s is delted!", id)
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": msg})
}

func GetUUIDfromString(uuid_string string, c *gin.Context) uuid.UUID {
	id, err := uuid.Parse(uuid_string)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": fmt.Sprintf("UUID Error: %w!", err)})
		return uuid.UUID{}
	}

	return id
}
