package controllers

import (
	"fmt"
	"net/http"
	"quiz_backend/db/postgres"
	"quiz_backend/models/question"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// QuestionController exported
type QuestionController struct{}

// GetQuestion exported
// Handler for GET [/question] -> /question?question_id="xxxx"
func (q *QuestionController) GetQuestion(c *gin.Context) {
	id := c.Query("question_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID, err := uuid.Parse(id)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err})
		return
	}

	obj, err := postgres.StoreObj.QuestionStore.Question(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// GetQuestionsByRoundID exported
// Handler for GET [/round/:round_id/question] ->
func (q *QuestionController) GetQuestionsByRoundID(c *gin.Context) {
	id := c.Param("round_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID, err := uuid.Parse(id)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	obj, err := postgres.StoreObj.QuestionStore.QuestionsByRoundID(uuidID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": obj})
}

// CreateQuestion exported
// Handler for POST [/question] -> data {"question_type": "xxx", "question_text": "xxx", "round_id": "xxx"}
func (q *QuestionController) CreateQuestion(c *gin.Context) {
	var f question.FormQuestion
	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	r, err := f.ToQuestion()
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Wrong input format!"})
		return
	}

	if err := postgres.StoreObj.QuestionStore.CreateQuestion(&r); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": r})
}

// UpdateQuestion exported
// Handler for PATCH [/question] -> data {...}
func (q *QuestionController) UpdateQuestion(c *gin.Context) {
	var f question.Question

	if err := c.BindJSON(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Could not read form input!"})
		return
	}

	if err := postgres.StoreObj.QuestionStore.UpdateQuestion(&f); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "response": f})
}

// DeleteQuestion exported
// Handler for DELETE [/question] -> /question?question_id=xxx
func (q *QuestionController) DeleteQuestion(c *gin.Context) {
	id := c.Query("question_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID := GetUUIDfromString(id, c)

	if err := postgres.StoreObj.QuestionStore.DeleteQuestion(uuidID); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	msg := fmt.Sprintf("Question %s is delted!", id)
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": msg})
}

// CheckQuestion exported
// Handler for GET [/question/checkquestion] -> /question/checkquestion?question_id=xxx
func (q *QuestionController) CheckQuestion(c *gin.Context) {
	id := c.Query("question_id")
	if id == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty id in query params."})
		return
	}

	uuidID := GetUUIDfromString(id, c)

	if err := postgres.StoreObj.AnswerStore.CheckIfAnswersAreValid(uuidID); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": "Question is correct!"})
}
