package controllers

import (
	"fmt"
	"log"
	"net/http"

	"quiz_backend/db/postgres"
	"quiz_backend/models/quiz"
	"quiz_backend/models/user"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

//UserController ...
type UserController struct{}

//GetUsers exported
func (ctrl UserController) GetUsers(c *gin.Context) {
	if qq, err := postgres.StoreObj.UserStore.Users(); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, []quiz.Quiz{})
	} else {
		c.JSON(http.StatusOK, gin.H{"ok": true, "response": qq})
	}
}

//DeleteUser with given UUID
func (ctrl UserController) DeleteUser(c *gin.Context) {
	userID := c.Query("user_id")
	if userID == "" {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": "Empty User id in query params."})
		return
	}

	uuidUser, err := uuid.Parse(userID)
	if err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err})
		return
	}

	if err := postgres.StoreObj.UserStore.DeleteUser(uuidUser); err != nil {
		c.JSON(http.StatusNotAcceptable, gin.H{"ok": false, "err": err.Error()})
		return
	}

	msg := fmt.Sprintf("User %s is delted!", userID)
	c.JSON(http.StatusOK, gin.H{"ok": true, "msg": msg})

}

//getUserID ...
func getUserID(c *gin.Context) (userID uuid.UUID) {
	//MustGet returns the value for the given key if it exists, otherwise it panics.
	return uuid.MustParse(c.MustGet("userID").(string))
}

//Login ...
func (ctrl UserController) Login(c *gin.Context) {
	var loginForm user.LoginForm

	if c.ShouldBindJSON(&loginForm) != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"message": "Invalid form", "form": loginForm})
		return
	}

	user, token, err := postgres.StoreObj.UserStore.Login(loginForm)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{"message": "User signed in", "user": user, "token": token})
	} else {
		c.JSON(http.StatusNotAcceptable, gin.H{"message": "Invalid login details", "error": err.Error()})
	}

}

//Register ...
func (ctrl UserController) Register(c *gin.Context) {
	var registerForm user.RegisterForm

	if c.ShouldBindJSON(&registerForm) != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"message": "Invalid form"})
		return
	}

	user, err := postgres.StoreObj.UserStore.Register(registerForm)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotAcceptable, gin.H{"message": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Successfully registered", "user": user})

}

//Logout ...
func (ctrl UserController) Logout(c *gin.Context) {

	au, err := authModel.ExtractTokenMetadata(c.Request)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"message": "User not logged in"})
		return
	}

	delErr := authModel.DeleteAuth(au.AccessUUID)
	if delErr != nil { //if any goes wrong
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "Invalid request"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Successfully logged out"})
}
