package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"quiz_backend/controllers"
	"quiz_backend/db/redis"

	"github.com/gin-contrib/gzip"
	"github.com/google/uuid"
	"github.com/joho/godotenv"

	"github.com/gin-gonic/gin"
)

//CORSMiddleware ...
//CORS (Cross-Origin Resource Sharing)
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "http://localhost")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding, x-access-token")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			fmt.Println("OPTIONS")
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}

//RequestIDMiddleware ...
//Generate a unique ID and attach it to each request for future reference or use
func RequestIDMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		uuid := uuid.New()
		c.Writer.Header().Set("X-Request-Id", uuid.String())
		c.Writer.Header().Set("Strict-Transport-Security", "max-age=63072000")
		c.Next()
	}
}

var auth = new(controllers.AuthController)

//TokenAuthMiddleware ...
//JWT Authentication middleware attached to each request that needs to be authenitcated to validate the access_token in the header
func TokenAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth.TokenValid(c)
		c.Next()
	}
}

func main() {

	//Start the default gin server
	r := gin.Default()

	//Load the .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file, please create one in the root directory")
	}

	//Embeed middleware
	r.Use(CORSMiddleware())
	r.Use(RequestIDMiddleware())
	r.Use(gzip.Gzip(gzip.DefaultCompression))

	redis.InitRedis("1")

	//Bind routes to router
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "Pong"})
	})

	api := r.Group("/api")
	{
		questCon := controllers.QuestionController{}
		answeCon := controllers.AnswerController{}

		/*** START USER ***/
		user := new(controllers.UserController)

		api.POST("/user/login", user.Login)
		api.POST("/user/register", user.Register)
		api.GET("/user/logout", user.Logout)

		/*** START AUTH ***/
		auth := new(controllers.AuthController)

		//Rerfresh the token when needed to generate new access_token and refresh_token for the user
		api.POST("/token/refresh", auth.Refresh)

		admin := api.Group("/admin", TokenAuthMiddleware())
		{
			admin.GET("/users", user.GetUsers)     // Get all users
			admin.DELETE("/user", user.DeleteUser) // Delete user
		}

		// quiz by user
		// api.GET("/user/:user_id/quizzes", controllers.GetQuizzesByUserId)

		// quiz
		quiz := api.Group("/quiz", TokenAuthMiddleware())
		{
			quiz.GET("", controllers.GetQuiz)
			quiz.GET("zes", controllers.GetQuizzes)
			quiz.POST("", controllers.PostQuiz)
			quiz.DELETE("", controllers.DeleteQuiz)
			quiz.PATCH("", controllers.UpdateQuiz)
			// round by quiz
			quiz.GET("/rounds", controllers.GetRoundsByQuizID)
		}

		// round
		round := api.Group("/round", TokenAuthMiddleware())
		{
			round.GET("", controllers.GetRound)
			round.GET("s", controllers.GetRounds)
			round.POST("", controllers.CreateRound)
			round.DELETE("", controllers.DeleteRound)
			round.PATCH("", controllers.UpdateRound)
			round.GET("/questions", questCon.GetQuestionsByRoundID)
		}

		// question
		quest := api.Group("/question", TokenAuthMiddleware())
		{
			//CRUD
			quest.GET("", questCon.GetQuestion)
			quest.POST("", questCon.CreateQuestion)
			quest.PATCH("", questCon.UpdateQuestion)
			quest.DELETE("", questCon.DeleteQuestion)
			// CheckQuestionType
			quest.GET("/checkquestion", questCon.CheckQuestion)
			// CheckAnswer
			quest.GET("/checkanswer", answeCon.CheckAnswer)
			quest.GET("/answers", answeCon.GetAnswersByQuestionID)
		}

		answe := api.Group("/answer", TokenAuthMiddleware())
		{
			answe.GET("", answeCon.CreateAnswer)
			answe.POST("", answeCon.CreateAnswer)
			answe.PATCH("", answeCon.UpdateAnswer)
			answe.DELETE("", answeCon.DeleteAnswer)
		}
	}

	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, gin.H{"msg": "Failure"})
	})

	// Auto configuration
	fmt.Println("SSL", os.Getenv("SSL"))
	port := os.Getenv("PORT")

	if os.Getenv("ENV") == "PRODUCTION" {
		gin.SetMode(gin.ReleaseMode)
	}

	if os.Getenv("SSL") == "TRUE" {

		SSLKeys := &struct {
			CERT string
			KEY  string
		}{}

		//Generated using sh generate-certificate.sh
		SSLKeys.CERT = "./cert/myCA.cer"
		SSLKeys.KEY = "./cert/myCA.key"

		r.RunTLS(":"+port, SSLKeys.CERT, SSLKeys.KEY)
	} else {
		r.Run(":" + port)
	}

}
