package game

import (
	"fmt"

	"github.com/google/uuid"
)

// FormGame is a struct to recive inputs from a json object
type FormGame struct {
	GameName string `json:"game_name"`
	State    int    `json:"state"`
	QuizID   string `json:"quiz_id"`
}

//ToGame converts an form entry to a Game struct
// Checks for GameType
func (f FormGame) ToGame() (Game, error) {
	if (f.State != 0) && (f.State != 1) && (f.State != 2) && (f.State != 3) {
		return Game{}, fmt.Errorf("GameType has to be in Array[0,1,2]")
	}

	uuidGame, err := uuid.Parse(f.QuizID)
	if err != nil {
		return Game{}, err
	}

	return NewGame(f.GameName, f.State, uuidGame), nil
}

// Game exported
type Game struct {
	ID         uuid.UUID `db:"id" json:"id"`
	GameName   string    `db:"game_name" json:"game_name"`
	State      int       `db:"state" json:"state"`
	QuizID     uuid.UUID `db:"quiz_id" json:"quiz_id"`
	RoundID    uuid.UUID `db:"round_id" json:"round_id"`
	RoundState int       `db:"round_state" json:"round_state"`
}

// NewGame is a function which takes the gameNAme, state and quizID and returns a new Game
func NewGame(gameName string, state int, quizID uuid.UUID) Game {
	return Game{
		GameName: gameName,
		State:    state,
		QuizID:   quizID,
	}
}

// GameStore Interface
type GameStore interface {
	Game(id uuid.UUID) (Game, error)
	Games() ([]Game, error)
	CreateGame(q *Game) error
	UpdateGame(q *Game) error
	DeleteGame(id uuid.UUID) error
}
