package quiz

import (
	"github.com/google/uuid"
)

type FormQuiz struct {
	Quizname string `json:"quiz_name"`
	Desc     string `json:"quiz_desc"`
	UserID   string `json:"user_id"`
	IsPublic bool   `json:"is_public"`
}

func (f FormQuiz) ToQuiz() (Quiz, error) {

	uuidParsed, err := uuid.Parse(f.UserID)
	if err != nil {
		return Quiz{}, err
	}

	return NewQuiz(f.Quizname, f.Desc, f.IsPublic, uuidParsed), nil
}

// User exported
type Quiz struct {
	ID       uuid.UUID `db:"id" json:"id"`
	Quizname string    `db:"quizname" json:"quizname"`
	Desc     string    `db:"description" json:"description"`
	UserID   uuid.UUID `db:"user_id" json:"user_id"`
	IsPublic bool      `db:"is_public" json:"is_public"`
}

func NewQuiz(quizname string, desc string, isPublic bool, userId uuid.UUID) Quiz {
	return Quiz{
		Quizname: quizname,
		Desc:     desc,
		UserID:   userId,
		IsPublic: isPublic,
	}
}

// UserStore Interface
type QuizStore interface {
	Quiz(id uuid.UUID) (Quiz, error)
	QuizzesByUserID(userID uuid.UUID) ([]Quiz, error)
	Quizzes() ([]Quiz, error)
	CreateQuiz(q *Quiz) error
	UpdateQuiz(q *Quiz) error
	DeleteQuiz(id uuid.UUID) error
}
