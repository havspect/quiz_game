package questiongame

import (
	"github.com/google/uuid"
)

// QuestionGame exported
type QuestionGame struct {
	ID         uuid.UUID `db:"id" json:"id"`
	UserGameID uuid.UUID `db:"user_game_id" json:"user_game_id"`
	QuestionID uuid.UUID `db:"question_id" json:"question_id"`
	AnswerID   uuid.UUID `db:"answer_id" json:"answer_id"`
}

// NewQuestionGame is a function which takes the gameNAme, state and quizID and returns a new Game
func NewQuestionGame(userGameID uuid.UUID, questionID uuid.UUID, answerID uuid.UUID) QuestionGame {
	return QuestionGame{
		UserGameID: userGameID,
		QuestionID: questionID,
		AnswerID:   answerID,
	}
}

// QuestionGameStore Interface
type QuestionGameStore interface {
	QuestionGame(id uuid.UUID) (QuestionGame, error)
	QuestionGames() ([]QuestionGame, error)
	CreateQuestionGame(q *QuestionGame) error
	UpdateQuestionGame(q *QuestionGame) error
	DeleteQuestionGame(id uuid.UUID) error
}
