package user

import (
	"quiz_backend/models/auth"
	"time"

	"github.com/google/uuid"
)

// User exported
type User struct {
	ID        uuid.UUID `db:"id" json:"id"`
	Username  string    `db:"username" json:"username"`
	Email     string    `db:"email" json:"email"`
	Password  string    `db:"password" json:"-"`
	IsAdmin   bool      `db:"is_admin" json:"-"`
	CreatedAt time.Time `db:"created_at" json:"created_at"`
	UpdatedAt time.Time `db:"updated_at" json:"updated_at"`
}

func NewUser(username string, email string, password string) User {
	return User{
		Username:  username,
		Email:     email,
		Password:  password,
		IsAdmin:   false,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}

//LoginForm ...
type LoginForm struct {
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required"`
}

//RegisterForm ...
type RegisterForm struct {
	Name     string `form:"name" json:"name" binding:"required,max=100"`
	Email    string `form:"email" json:"email" binding:"required,email"`
	Password string `form:"password" json:"password" binding:"required"`
}

// UserStore Interface
type UserStore interface {
	User(id uuid.UUID) (User, error)
	Users() ([]User, error)
	DeleteUser(id uuid.UUID) error
	Login(form LoginForm) (user User, token auth.Token, err error)
	Register(form RegisterForm) (user User, err error)
}
