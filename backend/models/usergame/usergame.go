package usergame

import (
	"github.com/google/uuid"
)

// UserGame exported
type UserGame struct {
	ID       uuid.UUID `db:"id" json:"-"`
	GameID   uuid.UUID `db:"game_id" json:"-"`
	UserID   uuid.UUID `db:"user_id" json:"-"`
	Points   int       `db:"points" json:"points"`
	Position int       `db:"position" json:"position"`
}

// NewUserGame is a function which takes the gameNAme, state and quizID and returns a new Game
func NewUserGame(gameID uuid.UUID, userID uuid.UUID, points int, position int) UserGame {
	return UserGame{
		GameID:   gameID,
		UserID:   userID,
		Points:   points,
		Position: position,
	}
}

// GameUserStore Interface
type GameUserStore interface {
	Game(id uuid.UUID) (UserGame, error)
	UserGamesbyGameID(gameID uuid.UUID) ([]UserGame, error)
	CreateUserGame(q *UserGame) error
	UpdateUserGame(q *UserGame) error
	DeleteUserGame(id uuid.UUID) error
}
