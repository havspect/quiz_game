package round

import (
	"github.com/google/uuid"
)

// FormRound is a struct to recive inputs from a json object
type FormRound struct {
	Roundname string `json:"round_name"`
	UserID    string `json:"user_id"`
	QuizID    string `json:"quiz_id"`
}

// ToRound converts an form entry to a round struct
func (f FormRound) ToRound() (Round, error) {
	uuidQuiz, err := uuid.Parse(f.QuizID)
	if err != nil {
		return Round{}, err
	}

	uuidUser, err := uuid.Parse(f.UserID)
	if err != nil {
		return Round{}, err
	}

	return NewRound(f.Roundname, uuidUser, uuidQuiz), nil
}

// Round exported
type Round struct {
	ID        uuid.UUID `db:"id" json:"id"`
	Roundname string    `db:"round_name" json:"round_name"`
	UserID    uuid.UUID `db:"user_id" json:"user_id"`
	QuizID    uuid.UUID `db:"quiz_id" json:"quiz_id"`
}

// NewRound is a function which takes the name, userID and QuizID and returns a new round
func NewRound(roundname string, userID uuid.UUID, quizID uuid.UUID) Round {
	return Round{
		Roundname: roundname,
		UserID:    userID,
		QuizID:    quizID,
	}
}

// RoundStore Interface
type RoundStore interface {
	Round(id uuid.UUID) (Round, error)
	RoundsByQuizID(quizID uuid.UUID) ([]Round, error)
	RoundsByUserID(userID uuid.UUID) ([]Round, error)
	Rounds() ([]Round, error)
	CreateRound(q *Round) error
	UpdateRound(q *Round) error
	DeleteRound(id uuid.UUID) error
}
