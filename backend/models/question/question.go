package question

import (
	"fmt"

	"github.com/google/uuid"
)

// FormQuestion is a struct to recive inputs from a json object
type FormQuestion struct {
	QuestionType     int    `json:"question_type"`
	QuestionText     string `json:"question_text"`
	QuestionCategory string `json:"question_category"`
	RoundID          string `json:"round_id"`
}

// ToQuestion converts an form entry to a Question struct
// Checks for QuestionType
func (f FormQuestion) ToQuestion() (Question, error) {
	if (f.QuestionType != 0) && (f.QuestionType != 1) && (f.QuestionType != 2) {
		return Question{}, fmt.Errorf("questionType has to be in Array[0,1,2]")
	}

	uuidRound, err := uuid.Parse(f.RoundID)
	if err != nil {
		return Question{}, err
	}

	return NewQuestion(f.QuestionType, f.QuestionText, f.QuestionCategory, uuidRound), nil
}

// Question exported
type Question struct {
	ID               uuid.UUID `db:"id" json:"id"`
	QuestionType     int       `db:"question_type" json:"question_type"`
	QuestionText     string    `db:"question_text" json:"question_text"`
	QuestionCategory string    `db:"question_category" json:"question_category"`
	RoundID          uuid.UUID `db:"round_id" json:"round_id"`
}

// NewQuestion is a function which takes the name, userID and QuizID and returns a new Question
func NewQuestion(questionType int, questionText string, questionCategory string, roundID uuid.UUID) Question {
	return Question{
		QuestionType:     questionType,
		QuestionText:     questionText,
		QuestionCategory: questionCategory,
		RoundID:          roundID,
	}
}

// QuestionStore Interface
type QuestionStore interface {
	Question(id uuid.UUID) (Question, error)
	QuestionsByRoundID(roundID uuid.UUID) ([]Question, error)
	Questions() ([]Question, error)
	CreateQuestion(q *Question) error
	UpdateQuestion(q *Question) error
	DeleteQuestion(id uuid.UUID) error
}
