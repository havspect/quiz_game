package models

import (
	"quiz_backend/models/answer"
	"quiz_backend/models/game"
	"quiz_backend/models/question"
	"quiz_backend/models/questiongame"
	"quiz_backend/models/quiz"
	"quiz_backend/models/round"
	"quiz_backend/models/user"
)

// Store interface exported
type Store interface {
	user.UserStore
	quiz.QuizStore
	round.RoundStore
	question.QuestionStore
	answer.AnswerStore
	game.GameStore
	questiongame.QuestionGameStore
}
