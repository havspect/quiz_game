package answer

import (
	"github.com/google/uuid"
)

// FormAnswer is a struct to recive inputs from a json object
type FormAnswer struct {
	AnswerText    string `json:"answer_text"`
	IsAnswerRight bool   `json:"is_answer_right"`
	QuestionID    string `json:"question_id"`
}

// ToAnswer converts an form entry to a Answer struct
func (f FormAnswer) ToAnswer() (Answer, error) {

	uuidQuestion, err := uuid.Parse(f.QuestionID)
	if err != nil {
		return Answer{}, err
	}

	return NewAnswer(f.AnswerText, f.IsAnswerRight, uuidQuestion), nil
}

// Answer exported
type Answer struct {
	ID            uuid.UUID `db:"id" json:"id"`
	AnswerText    string    `db:"answer_text" json:"answer_text"`
	IsAnswerRight bool      `db:"is_answer_right" json:"-"`
	QuestionID    uuid.UUID `db:"question_id" json:"question_id"`
}

// NewAnswer is a function which takes the name, userID and QuizID and returns a new Answer
func NewAnswer(answerText string, isAnswerRight bool, questionID uuid.UUID) Answer {
	return Answer{
		AnswerText:    answerText,
		IsAnswerRight: isAnswerRight,
		QuestionID:    questionID,
	}
}

// AnswerStore Interface
type AnswerStore interface {
	Answer(id uuid.UUID) (Answer, error)
	AnswersByQuestionID(questionID uuid.UUID) ([]Answer, error)
	Answers() ([]Answer, error)
	CreateAnswer(q *Answer) error
	UpdateAnswer(q *Answer) error
	DeleteAnswer(id uuid.UUID) error
	//Question
	CheckIfAnswersAreValid(questionID uuid.UUID) error
	CheckIfAnswerIsCorrect(questionID uuid.UUID, answerID uuid.UUID) (bool, error)
}
